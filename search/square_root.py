def square_root(n):
    answer = -1

    ''' binary search for square root '''
    lower = 1
    upper = n
    while upper - lower > 1:
        middle = (upper + lower) / 2
        temp = middle * middle
        #print lower, upper, temp
        if temp == n:
            print middle
            answer = middle
            break

        if temp > n:
            upper =  middle
        else:
            lower = middle


    if answer == -1:
        print '%d has no square root' % n
    else:
        print '%d' % (answer)


'''
n=81
1,81,41
1,41,21
'''
if __name__ == '__main__':
    square_root(1024)

