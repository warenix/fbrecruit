def merge_sort(array, start, end):
    l = end - start

    if l <= 1:
        return [array[start]]

    mid = ((start + end) / 2)
    left = merge_sort(array, start, mid)
    right = merge_sort(array, mid, end)

    print "left"
    print_line(left)
    print "right"
    print_line(right)
    print "---"

    iLeft = 0
    sizeLeft = len(left)
    iRight = 0
    sizeRight = len(right)
    result = []
    while iLeft < sizeLeft and iRight < sizeRight:
        if left[iLeft] < right[iRight]:
            result.append(left[iLeft])
            print "copy left %d" % left[iLeft]
            iLeft += 1
        elif right[iRight] <= left[iLeft]:
            result.append(right[iRight])
            print "copy right %d" % right[iRight]
            iRight += 1

    while iLeft < sizeLeft:
        result.append(left[iLeft])
        print "blind copy left %d" % left[iLeft]
        iLeft += 1

    while iRight < sizeRight:
        result.append(right[iRight])
        print "blind copy right %d" % right[iRight]
        iRight += 1

    #print_line(result)
    return result

def print_line(array):
    line = ""
    for a in array:
        line += str(a) + " "

    print line

if __name__ == '__main__':
    l = [6,5,3,1,8,7,2,4]
    #l = [6,5,3]
    lSorted = merge_sort(l,0,len(l))
    print_line(lSorted)

