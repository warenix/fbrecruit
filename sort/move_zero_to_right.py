# Given an array of integers.
# Move all non-zero elements to the left of all zero elements.
# http://www.careercup.com/question?id=5668212962230272

def move_array(array):
    ''' assume
    contains negative number
    e.g. 1,0,6,0,-4,2,5,1
    out: 1,6,-4,2,5,1,0,0
    '''

    l = len(array)
    i = 0
    while i < l:
        # try to move i-th element to the right
        if array[i] == 0:
            shift_array(array, i)
        i+=1

    print_line(array)

def shift_array(array, i):
    l = len(array)
    j = i + 1
    while j < l:
        tmp = array[j]
        array[j] = array[j-1]
        array[j-1] = tmp
        j += 1


def print_line(array):
    line = ""
    for a in array:
        line += str(a) + " "
    print line

if __name__ == '__main__':
    move_array([1,0,6,0,-4,2,5,1])
