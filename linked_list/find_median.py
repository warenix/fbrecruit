class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class IncreasingOrderLinkedList:
    def __init__(self):
        self.head = None
        self.size = 0
        
    def addNode(self, node):
        if self.head is None:
            # add at head
            self.head = node
        else:
            # this node points to a node with value just smaller than the node being added
            nodePrevious = None
            nodeCurrent = self.head
            # move node current to the node that is just greater than the node being added
            while nodeCurrent is not None:
                if nodeCurrent.value > node.value:
                    break
                nodePrevious = nodeCurrent
                nodeCurrent = nodeCurrent.next

            if nodeCurrent is None:
                nodePrevious.next = node
            else:
                node.next = nodeCurrent
                if nodePrevious is None:
                    self.head = node
                else:
                    nodePrevious.next = node
                
        self.size += 1
                
    def median(self):
        '''
        return the middle node. None when theres no node in the list
        '''
        
        # point to current node
        nodeCurrent = self.head
        step = 0
        middle = (self.size + 1) / 2
        
        while nodeCurrent is not None:
            if step == middle:
                break
            step += 1
            nodeCurrent = nodeCurrent.next
            
        return nodeCurrent
        
class SampleHandler:

    def __init__(self):
        self.dataStore = IncreasingOrderLinkedList()
    
    def addNumber(self, number):
        '''
        add a number to the data structure
        '''
        self.dataStore.addNode(Node(number))
        
    def median(self):
        '''
        return the median of all numbers holded
        '''
        nodeMedian = self.dataStore.median()
        if nodeMedian is not None:
            return nodeMedian.value
        else:
            print 'no median'
            
t = SampleHandler()           
for i in [2,7,4,9,1,5,8,3,6]:
    t.addNumber(i)
print t.median()
