class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
        
class LinkedList:
    def __init__(self):
        self.head = None

    def printList(self):
        line = None
        nodeTemp = self.head
        while nodeTemp is not None:
            if line is None:
                line = nodeTemp.value
            else:
                line += "," + nodeTemp.value

            nodeTemp = nodeTemp.next

        print line


    def push(self, node):
        ''' add a node on left '''
        if self.head is None:
            self.head = node
        else:
            nodeTemp = self.head
            self.head = node
            node.next = nodeTemp
        
    def pushBack(self, node):
        ''' add a node on right '''
        
        if self.head is None:
            self.head = node
        else:
            nodeTemp = self.head
            while nodeTemp.next is not None:
                nodeTemp = nodeTemp.next
            nodeTemp.next = node
            node.next = None
        
        
    def count(self):
        ''' return no. of nodes in this list '''
        l = 0
        nodeTemp = self.head
        while nodeTemp is not None:
            l += 1
            nodeTemp = nodeTemp.next
            
        return l
        
    def getNth(self, n):
        ''' return the n-th node in the list '''
        node = self.head
        counterN = 0
        while node is not None:
            if counterN == n:
                break
                
            counterN += 1
            node = node.next
            
        if counterN == n:
            return node
        else:
            return None #n is greater then the size of this list
            
    def deleteList(self):
        nodeTemp = self.head
        while nodeTemp is not None:
            nodeCurrent = nodeTemp
            nodeTemp = nodeTemp.next
            # free current
            nodeCurrent.value = None
            nodeCurrent.next = None
        
        self.head = None
            

    def pop(self):
        ''' remove the head node and return the node data '''
        if self.head is None:
            return None
            
        nodeTemp = self.head
        self.head = nodeTemp.next
        
        # free 'head'
        nodeTemp.next = None
        return nodeTemp.value
        
    def insertNth(self, node, n):
        ''' insert node at nth position '''
        
        nodePrevious = None
        nodeTemp = self.head
        counterN = 0
        
        while nodeTemp is not None:
            if counterN == n:
                break
            nodePrevious = nodeTemp
            nodeTemp = nodeTemp.next
            counterN += 1
            
        if counterN == n:
            if nodePrevious is None:
                if self.head is None:
                    # list is empty
                    self.head = node
                else:
                    # add at position 0
                    node.next = self.head
                    self.head = node
            else:
                nodePrevious.next = node
                node.next = nodeTemp
            
              
if __name__ == '__main__':  
    sll = LinkedList()
    
    i = 0
    while i < 5:
        node = Node(str(i))
        sll.push(node)
        i += 1
    
    i = 10
    while i < 15:
        node = Node(str(i))
        sll.pushBack(node)
        i += 1
    
    
    print sll.count()    
    sll.printList() # 4,3,2,1,0,10,11,12,13,14

    print sll.getNth(0).value # expect 0
    print sll.getNth(4).value # expect 5
    print sll.getNth(5).value # expect 10
    print sll.getNth(9).value # expect 14
    print sll.getNth(20) # expect None
    print sll.getNth(-20) # expect None

    print sll.pop() # expect 4
    print sll.count() # expect 9
    print sll.pop() # expect 3
    print sll.count() # expect 8
    
    sll.deleteList()
    print sll.count() # expect 0
    
    node = Node('20')
    sll.insertNth(node, 2)
    node = Node('21')
    sll.insertNth(node, 0)
    node = Node('22')
    sll.insertNth(node, 2)
    sll.printList()
    
