class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

    def setNext(self, next):
        self.next = next

def check_circular(l):
    ''' return true if circular, otherwise false'''
    visited = []
    
    node = l
    while True:
        if node is None:
            return False
        if node in visited:
            return True

        visited.append(node)        
        node = node.next

# test        
a = Node('a')
b = Node('b')
c = Node('c')
d = Node('d')

a.setNext(b)
b.setNext(c)
b.setNext(d)
l = a
print check_circular(l) # false


a.setNext(b)
b.setNext(c)
b.setNext(d)
d.setNext(b)
l = a
print check_circular(l) # true

