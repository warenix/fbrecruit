# linked list

class Node:
    def __init__(self, value):
        self.next = None
        self.value = value

    def setNext(self, node):
        self.next = node

# create a linked list
a = Node('a')
b = Node('b')
c = Node('c')
d = Node('d')
a.setNext(b)
b.setNext(c)
c.setNext(d)

l = a

# simple transverse
def transverse(l):
    n = l
    while True:
        if n is None:
            break
        print n.value
        n = n.next
    print
    print

# now reverse linked list in a loop
def reverse(l):
    node = l
    previous = None
    while True:
        if node is None:
            l = previous
            break

        temp_node = node.next
        node.next = previous
        previous = node
        node = temp_node
    return l

transverse(l)
l = reverse(l)
transverse(l)
