import collections
import sys

class Node:
    def __init__(self, value, left, right):
        self.value = value
        self.left = left
        self.right = right

    def set_depth(self, depth):
        self.depth = depth

def dfs(node, stack, visited):
    ''' traditional dfs written in loop '''
    print sys._getframe().f_code.co_name

    stack.appendleft(node)
    while len(stack) > 0:
        n = stack.pop()
        print n.value

        if n.value not in visited:
            visited[n.value] = True

            if n.right is not None:
                stack.append(n.right)
            if n.left is not None:
                stack.append(n.left)

def bfs(node, q, visited):
    ''' traditional bfs written in loop'''
    print sys._getframe().f_code.co_name

    q.append(node)
    while len(q) > 0:
        n = q.popleft()
        print n.value

        if n.value not in visited:
            visited[n.value] = True
            if n.left != None:
                q.append(n.left)
            if n.right != None:
                q.append(n.right)

def bfs_per_depth(node, q, visited):
    ''' print node of the same depth in one line

    http://www.glassdoor.com/Interview/Print-a-binary-tree-level-per-level-1-2-3-4-5-6-7-Would-print-1-23-4567-QTN_788354.htm
    '''
    print sys._getframe().f_code.co_name

    buff = []
    last_print_depth = 0
    node.set_depth(1)
    q.append(node)
    buff.append(node)
    while len(q) > 0:
        n = q.popleft()
        if n.depth != last_print_depth:
            last_print_depth = n.depth
            print ''.join(v.value for v in buff)
            del buff[:]

        if n.value not in visited:
            visited[n.value] = True
            if n.left != None:
                n.left.set_depth(n.depth + 1)
                q.append(n.left)
                buff.append(n.left)
            if n.right != None:
                n.right.set_depth(n.depth + 1)
                q.append(n.right)
                buff.append(n.right)

if __name__ == '__main__':
    '''
    sample binary tree:
        c
       a  e
     b   d  f
           g
          h
    '''
    h = Node('h', None, None)
    g = Node('g', h, None)
    f = Node('f', g, None)
    d = Node('d', None, None)
    b = Node('b', None, None)
    e = Node('e', d, f)
    a = Node('a', b, None)
    c = Node('c', a, e)
    visited = {}

    visited.clear()
    bfs(c, collections.deque(), visited)
    visited.clear()
    dfs(c, collections.deque(), visited)
    visited.clear()
    bfs_per_depth(c, collections.deque(), visited)
