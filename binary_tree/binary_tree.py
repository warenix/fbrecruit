class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        
        
def print_path(node, path):
    ''' print root-to-leaf path from the given node '''
    if node is None:
        return
    else:
        path.append(node)
        
        if node.left is None and node.right is None:                    
            # reached the leaf, print path if the tree is not empty
            if path is not None and len(path) > 0:
                line = ""
                for n in path:
                    line += str(n.value) + ","
                print line
    
        print_path(node.left, path)
        print_path(node.right, path)
        path.remove(node)
            
            
if __name__ == '__main__':
    a = Node(5)
    b = Node(4)
    c = Node(11)
    d = Node(7)
    e = Node(8)
    f = Node(13)
    g = Node(4)
    h = Node(1)
    i = Node(2)
    
    a.left = b
    a.right = e
    b.left = c
    c.left = d
    c.right = i
    e.left = f
    e.right = g
    g.right = h
    
    print_path(a, [])
    
