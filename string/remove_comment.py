def remove_comment(array):
    sizeArray = len(array)
    
    isCommentStarted = False
    flagStar = False
    
    OPEN_COMMENT_1 = 1 # "/*", / is seen
    OPEN_COMMENT_2 = 2 # "/*", * is also seen
    CLOSE_COMMENT_1 = 3 # "*/", * is seen
    CLOSE_COMMENT_2 = 4 # "*/", / is also seen
    state = CLOSE_COMMENT_2
    
    i = 0
    while i < sizeArray:
        line = array[i]
        sizeLine = len(line)
        output = ""
        for c in line:
            if state == CLOSE_COMMENT_2:
                if '/' == c:
                    state = OPEN_COMMENT_1
                else:
                    output += c
            elif state == OPEN_COMMENT_1:
                if '*' == c:
                    state = OPEN_COMMENT_2
                else:
                    output += '/' # this comes form OPEN_COMMENT_1
                    output += c
            elif state == OPEN_COMMENT_2:
                if '*' == c:
                    state = CLOSE_COMMENT_1
                else:
                    pass #output += c
            elif state == CLOSE_COMMENT_1:
                if '/' == c:
                    state = CLOSE_COMMENT_2
                else:
                    state = OPEN_COMMENT_2                
                    

        if len(output) > 0:        
            print output
        i+=1
    
def print_line(array):
    line = ""
    for a in array:
        line += str(a) + " "
    print line
    
if __name__ == '__main__':
    lines = []
    lines.append("hello /* this is a")
    lines.append("hello /* this is a")
    lines.append("mulit line comment */ all")
    remove_comment(lines)
