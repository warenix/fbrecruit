def fib(n):
    if n < 1:
        return 0
    if n == 1:
        return 1
    return fib(n-2) + fib(n-1)


def fib_loop(n):
    if n < 1:
        return 0
    if n == 1:
        return 1

    a = 0
    b = 1
    i = 2
    total = 0
    while i <= n:
        total = a + b
        a = b
        b = total
        i += 1
    return total

print fib(0)
print fib(1)
print fib(2)
print fib(5)
print fib(7)

print fib_loop(0)
print fib_loop(1)
print fib_loop(2)
print fib_loop(5)
print fib_loop(7)
