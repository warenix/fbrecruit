def reverse(s):
    l = len(s)
    rstr = ""
    i = l-1
    while i >= 0:
        rstr += s[i]
        i-=1
    return rstr

def reverse_swap(s):
    l = len(s)
    rstr = bytearray(s)
    middle = l/2
    i = 0
    while i < middle:
        t = rstr[i]
        rstr[i] = rstr[l-1-i]
        rstr[l-1-i] = t
        i+=1

    return rstr

print reverse("hello world")
print reverse("")
print reverse("a")

print reverse_swap("hello world")
print reverse_swap("")
print reverse_swap("a")
