import sys

def find_max(array):
    max_item = -sys.maxint-1
    
    n = 0
    l = len(array)
    i = 0
    while i < l:
        n = array[i]
        if max_item < n:
            max_item = n
        i += 1
    return max_item


print find_max([1,2,3,sys.maxint, 4,5])
