def checkSubString(s1, pos1, s2, pos2):
    '''
    take a character from s2, then try to match corresponding in s1
    return true if s2 is substring of s1
    '''
    size1 = len(s1)
    size2 = len(s2)
    
    if pos2 == size2:
        # consumed whole s2, now see we can reach the end of s1
        return pos1 == size1
    
    c1 = s1[pos1]
    c2 = s2[pos2]
    if c2 == '?':
        return checkSubString(s1, pos1+1, s2, pos2+1)
    elif c2 == '*':
        while pos1 < size1:
            if checkSubString(s1, pos1+1, s2, pos2+1):
                return True
            pos1+=1
        return False
    else:
        if c1 == c2:
            return checkSubString(s1, pos1+1, s2, pos2+1)
        else:
            return False
    
print checkSubString('abc', 0, 'abc', 0)
print checkSubString('abc', 0, 'a*c', 0)
print checkSubString('abc', 0, 'ac', 0)
print checkSubString('abc', 0, 'a?', 0)
print checkSubString('abc', 0, '?b?', 0)
print checkSubString('abc', 0, '**', 0)

