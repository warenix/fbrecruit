def multi_table(n):
    if n < 1:
        return

    r = 1
    c = 1

    while r <= n:
        c = 1
        line = ""
        while c <= n:
            # print each cell
            product = r * c
            line += "%4d" % (product)
            c += 1
        print line
        r+=1

multi_table(0)
multi_table(12)



