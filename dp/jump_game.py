def check_can_reach_end(array):
    ''' return true if can reach the end from the beginning'''

    sizeArray = len(array)
    visited = [0] * sizeArray # visited[i] means whether we can reach element i in the array, 0:false 1:true

    visited[0] = 1 # mark it ture since we start from the first element
    i = 0; # current hop
    hasReachedEnd = False
    while i < sizeArray and not hasReachedEnd:
        if visited[i] == 1:
            j = 1
            while j <= array[i]:
                if i + j < sizeArray:
                    visited[i + j] = 1

                    if i + j == sizeArray - 1:
                        hasReachedEnd = True
                        break
                else:
                    break
                j += 1
        i += 1

    return visited[sizeArray-1] == 1

def print_line(array):
    line = ""
    for a in array:
        line += str(a) + " "
    print line
if __name__ == '__main__':
    l = [1,2,0,1,0,1]
    print check_can_reach_end(l)

    l = [2,2,0,1]
    print check_can_reach_end(l)

